from django.contrib import admin
from .models import Brand, ComponentType

admin.site.register(Brand)
admin.site.register(ComponentType)
