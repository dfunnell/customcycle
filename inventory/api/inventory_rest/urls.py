from django.urls import path

from .views import (
    api_brand, api_component_type
)

urlpatterns = [
    path(
        "brand/",
        api_brand,
        name="api_brand",
    ),
        path(
        "component_type/",
        api_component_type,
        name="api_component_type",
    ),
]
