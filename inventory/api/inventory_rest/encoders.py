from common.json import ModelEncoder

from .models import Brand, ComponentType


class BrandEncoder(ModelEncoder):
    model = Brand
    properties = [
        "id",
        "name",
    ]

class ComponentTypeEncoder(ModelEncoder):
    model = ComponentType
    properties = [
        "id",
        "name",
    ]
