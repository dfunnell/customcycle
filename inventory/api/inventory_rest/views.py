from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .encoders import BrandEncoder, ComponentTypeEncoder
from .models import Brand, ComponentType



@require_http_methods(["GET", "POST"])
def api_brand(request):
    if request.method == "GET":
        brands = Brand.objects.all()
        return JsonResponse(
            {"brands": brands},
            encoder=BrandEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            brand = Brand.objects.create(**content)
            return JsonResponse(
                brand,
                encoder=BrandEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the brand"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "POST"])
def api_component_type(request):
    if request.method == "GET":
        component_types = ComponentType.objects.all()
        return JsonResponse(
            {"component_types": component_types},
            encoder=ComponentTypeEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            component_type = ComponentType.objects.create(**content)
            return JsonResponse(
                component_type,
                encoder=ComponentTypeEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the component type"}
            )
            response.status_code = 400
            return response
