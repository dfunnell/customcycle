from django.db import models
from django.urls import reverse


class Brand(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_brand", kwargs={"pk": self.id})


class ComponentType(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_brand", kwargs={"pk": self.id})
