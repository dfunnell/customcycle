import React, { useState } from "react";

function BrandForm() {
  const [name, setName] = useState("");
  const [created, setCreated] = useState("");

  function handleNameChange(event) {
    const value = event.target.value;
    setName(value);
    setCreated("");
  }

  async function handleSubmit(event) {
    event.preventDefault();
    const data = { name };
    const brandUrl = "http://localhost:8100/api/brand/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(brandUrl, fetchConfig);
    if (response.ok) {
      setName("");
      setCreated("created");
    } else {
      setCreated("error");
    }
  }
  let messageErrorClasses = "alert alert-success d-none mb-0";
  let messageSuccessClasses = "alert alert-success d-none mb-0";
  if (created === "created") {
    messageSuccessClasses = "alert alert-success mt-3 mb-0";
  } else if (created === "error") {
    messageErrorClasses = "alert alert-danger mt-3 mb-0";
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className=" p-3 mt-3">
          <h1 className="text-center">Add A Brand</h1>
          <form onSubmit={handleSubmit} id="create-location-form">
            <div className="form-floating mb-3 d-flex justify-content-between">
              <input
                value={name}
                onChange={handleNameChange}
                placeholder="name"
                required
                type="text"
                name="name"
                id="name"
                className="form-control"
              />
              <label htmlFor="name">Name</label>
              <button className="btn btn-primary ms-2">Create</button>
            </div>
          </form>
          <div className={messageSuccessClasses} id="success-message">
            Congratulations! You added a brand!
          </div>
          <div className={messageErrorClasses} id="error-message">
            Warning, you encountered an error! That brand may already exist in the
            database.
          </div>
        </div>
      </div>
    </div>
  );
}

export default BrandForm;
