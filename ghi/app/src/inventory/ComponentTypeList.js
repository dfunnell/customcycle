import React, { useEffect, useState } from "react";

function ComponentTypeList() {
  const [componentTypes, setBrands] = useState({ data: [] });

  useEffect(() => {
    const url = "http://localhost:8100/api/component_type/";
    async function fetchData() {
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        console.log(data)
        setBrands({ data: data.component_types });
      }
    }
    fetchData();
  }, []);

  return (
    <div className="container">
      <div className="row justify-content-center mt-4">
        <div className="col-md-6">
          <h1 className="text-center">Brand List</h1>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Name</th>
              </tr>
            </thead>
            <tbody>
              {componentTypes.data.map((componentType) => (
                <tr key={componentType.name}>
                  <td>{componentType.name}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}

export default ComponentTypeList;
