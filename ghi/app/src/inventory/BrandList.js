import React, { useEffect, useState } from "react";

function BrandList() {
  const [brands, setBrands] = useState({ data: [] });

  useEffect(() => {
    const url = "http://localhost:8100/api/brand/";
    async function fetchData() {
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setBrands({ data: data.brands });
      }
    }
    fetchData();
  }, []);

  return (
    <div className="container">
      <div className="row justify-content-center mt-4">
        <div className="col-md-6">
          <h1 className="text-center">Brand List</h1>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Name</th>
              </tr>
            </thead>
            <tbody>
              {brands.data.map((brand) => (
                <tr key={brand.name}>
                  <td>{brand.name}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}

export default BrandList;
