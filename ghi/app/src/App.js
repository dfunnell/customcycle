import { Component } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import BrandForm from "./inventory/BrandForm";
import BrandList from "./inventory/BrandList";
import ComponentTypeForm from "./inventory/ComponentTypeForm";
import ComponentTypeList from "./inventory/ComponentTypeList";

export default class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Nav />
        <div className="container">
          <Routes>
            <Route path="/" element={<MainPage />} />
            <Route path="inventory">
              <Route path="brand_form" element={<BrandForm />} />
              <Route path="brand_list" element={<BrandList />} />
              <Route path="component_type_form" element={<ComponentTypeForm />} />
              <Route path="component_type_list" element={<ComponentTypeList />} />
            </Route>
          </Routes>
        </div>
      </BrowserRouter>
    );
  }
}
